package com.ongthe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class EmployeeRoleMapper implements RowMapper<EmployeeRole> {

	@Override
	public EmployeeRole mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeRole empRole = new EmployeeRole();
		empRole.setRoleId(rs.getInt("employeeroleid"));
		empRole.setRoleName(rs.getString("employeerolename"));
		
		return empRole;
	}

}