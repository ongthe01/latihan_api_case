package com.ongthe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ongthe.model.*;

@Transactional
@Repository
public class EmployeeDao {

	@Autowired
	private JdbcTemplate jdbc;

	public List<Employee> getAll() {
		String sqlGetAllEmp = "SELECT * FROM employee ORDER BY employeeId asc";
		List<Employee> employee = jdbc.query(sqlGetAllEmp, new EmployeeMapper());
		return employee;
	}

	public Employee getEmployeeById(int id) {
		String sqlGetEmpId = "SELECT * FROM employee WHERE employeeId = ?";
		Employee employee = jdbc.queryForObject(sqlGetEmpId, new Object[] { id }, new EmployeeMapper());
		return employee;
	}

	// add employee
	public void addEmployee(Employee employee) {
		String sqlAddEmp = "INSERT INTO employee (employeeroleid, employeename, phone, address) VALUES (?,?,?,?)";
		jdbc.update(sqlAddEmp, employee.getRoleId(), employee.getName(), employee.getPhone(), employee.getAddress());
	}

	// update employee
	public void updateEmployee(Employee employee, int id) {
		String sqlUpdateEmp = "UPDATE employee SET employeeroleid=?, employeename=?, phone=?, address=? WHERE employeeid=?";
		jdbc.update(sqlUpdateEmp, employee.getRoleId(), employee.getName(), employee.getPhone(), employee.getAddress(),
				id);
	}

	// delete employee
	public void deleteEmployeeById(int id) {
		String sqlDelEmp = "DELETE FROM employee WHERE employeeid=?";
		jdbc.update(sqlDelEmp, id);
	}

	// get lastest employee
	public int latestInput() {
		String sqlLastInputEmp = "SELECT currval(pg_get_serial_sequence('employee','employeeid'))";
		int id = jdbc.queryForObject(sqlLastInputEmp, Integer.class);
		return id;
	}
}