package com.ongthe.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ongthe.model.*;

@Transactional
@Repository
public class EmployeeRoleDao {

	@Autowired
	private JdbcTemplate jdbc;

	public List<EmployeeRole> getAll() {
		String sqlGetAllRole = "SELECT * FROM employeeRole ORDER BY employeeroleid asc";
		List<EmployeeRole> employeeRole = jdbc.query(sqlGetAllRole, new EmployeeRoleMapper());
		return employeeRole;
	}

	// get employee role by id
	public EmployeeRole getById(int roleId) {
		String sqlRoleById = "SELECT * FROM employeerole WHERE employeeroleid = ?";
		EmployeeRole empRole = jdbc.queryForObject(sqlRoleById, new Object[] { roleId }, new EmployeeRoleMapper());
		return empRole;
	}

	// add employee role
	public void addEmployeeRole(EmployeeRole empRole) {
		String sqlAddEmployee = "INSERT INTO employeerole (employeerolename) VALUES (?)";
		jdbc.update(sqlAddEmployee, empRole.getRoleName());
	}

	// update employee role
	public void updateEmployeeRole(EmployeeRole empRole, int roleId) {
		String sqlUpdateRole = "UPDATE employeerole SET employeerolename=? WHERE employeeroleid=?";
		jdbc.update(sqlUpdateRole, empRole.getRoleName(), roleId);
	}

	// delete employee role
	public void deleteEmployeeRoleById(int roleId) {
		String sqlDelRole = "DELETE FROM employeerole WHERE employeeroleid=?";
		jdbc.update(sqlDelRole, roleId);
	}

	public int latestInput() {
		String sqlLastInput = "SELECT currval(pg_get_serial_sequence('employeerole','employeeroleid'))";
		int roleId = jdbc.queryForObject(sqlLastInput, Integer.class);
		return roleId;
	}
}