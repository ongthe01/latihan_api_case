package com.ongthe.service;

import java.util.List;
import com.ongthe.model.*;

public interface EmployeeService {
	List<Employee> getAll();
	
	Employee getEmployeeById(int id);

	void addEmployee(Employee employee);

	void updateEmployee(Employee employee, int id);

	void deleteEmployee(int id);

	int lastestInputEmployee();

}