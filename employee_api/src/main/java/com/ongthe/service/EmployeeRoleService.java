package com.ongthe.service;

import java.util.List;

import com.ongthe.model.*;

public interface EmployeeRoleService {
	List<EmployeeRole> getAll();
	
	EmployeeRole getEmployeeRoleById(int roleId);

	void addEmployeeRole(EmployeeRole empRole);

	void updateEmployeeRole(EmployeeRole empRole, int roleId);

	void deleteEmployeeRole(int roleId);

	int lastestInputEmployeeRole();
}