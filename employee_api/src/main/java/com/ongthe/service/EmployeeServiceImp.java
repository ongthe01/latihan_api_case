package com.ongthe.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ongthe.dao.*;
import com.ongthe.model.*;

@Service
public class EmployeeServiceImp implements EmployeeService{

	@Autowired
	EmployeeDao employeeDao;
	
	@Override
	public List<Employee> getAll() {
		return employeeDao.getAll();
	}

	@Override
	public Employee getEmployeeById(int id) {
		return employeeDao.getEmployeeById(id);
	}

	//add employee
	@Override
	public void addEmployee(Employee employee) {
		employeeDao.addEmployee(employee);
	}

	//update employee
	@Override
	public void updateEmployee(Employee employee, int id) {
		employeeDao.updateEmployee(employee, id);
	}

	//delete employee
	@Override
	public void deleteEmployee(int id) {
		employeeDao.deleteEmployeeById(id);
	}

	@Override
	public int lastestInputEmployee() {
		return employeeDao.latestInput();
	}

}