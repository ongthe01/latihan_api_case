package com.ongthe.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ongthe.dao.*;
import com.ongthe.model.*;

@Service
public class EmployeeRoleServiceImp implements EmployeeRoleService{


	@Autowired
	EmployeeRoleDao empRoleDao;
	
	@Override
	public List<EmployeeRole> getAll() {
		return empRoleDao.getAll();
	}

	@Override
	public EmployeeRole getEmployeeRoleById(int roleId) {
		return empRoleDao.getById(roleId);
	}

	@Override
	public void addEmployeeRole(EmployeeRole empRole) {
		empRoleDao.addEmployeeRole(empRole);
	}

	@Override
	public void updateEmployeeRole(EmployeeRole empRole, int roleId) {
		empRoleDao.updateEmployeeRole(empRole, roleId);
	}

	@Override
	public void deleteEmployeeRole(int roleId) {
		empRoleDao.deleteEmployeeRoleById(roleId);
	}

	@Override
	public int lastestInputEmployeeRole() {
		return empRoleDao.latestInput();
	}

}