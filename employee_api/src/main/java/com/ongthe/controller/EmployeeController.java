package com.ongthe.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ongthe.model.*;
import com.ongthe.service.*;

@RequestMapping("employee")
@Controller
public class EmployeeController {
	@Autowired
	EmployeeService empServ;

	@GetMapping("/{id}")
	public ResponseEntity<Employee> getById(@PathVariable("id") int id) {
		Employee emp = empServ.getEmployeeById(id);
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<Employee>> getAll() {
		List<Employee> listEmployee = empServ.getAll();
		return new ResponseEntity<List<Employee>>(listEmployee, HttpStatus.OK);
	}

	// Add
	@PostMapping("/")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {

		empServ.addEmployee(employee);
		Employee emp = empServ.getEmployeeById(empServ.lastestInputEmployee());

		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}

	// Update
	@PutMapping("/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {

		empServ.updateEmployee(employee, id);
		Employee emp = empServ.getEmployeeById(id);

		return new ResponseEntity<Employee>(emp, HttpStatus.OK);

	}

	// Delete
	@DeleteMapping("/{id}")
	public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") int id) {

		Employee emp = empServ.getEmployeeById(id);
		empServ.deleteEmployee(id);

		return new ResponseEntity<Employee>(emp, HttpStatus.OK);

	}
}