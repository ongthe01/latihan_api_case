package com.ongthe.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ongthe.model.*;
import com.ongthe.service.*;

@RequestMapping("employeeRole")
@Controller
public class EmployeeRoleController {

	@Autowired
	EmployeeRoleService empRoleService;

	@GetMapping("/{id}")
	public ResponseEntity<EmployeeRole> getById(@PathVariable("id") int roleId) {

		EmployeeRole employeeRole = empRoleService.getEmployeeRoleById(roleId);

		return new ResponseEntity<EmployeeRole>(employeeRole, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<EmployeeRole>> getAll() {
		List<EmployeeRole> listEmployeeRole = empRoleService.getAll();
		return new ResponseEntity<List<EmployeeRole>>(listEmployeeRole, HttpStatus.OK);
	}

	// Add
	@PostMapping("/")
	public ResponseEntity<EmployeeRole> addEmployeeRole(@RequestBody EmployeeRole empRole) {

		empRoleService.addEmployeeRole(empRole);
		EmployeeRole employeeRole = empRoleService.getEmployeeRoleById(empRoleService.lastestInputEmployeeRole());

		return new ResponseEntity<EmployeeRole>(employeeRole, HttpStatus.OK);

	}

	// Update
	@PutMapping("/{id}")
	public ResponseEntity<EmployeeRole> updateEmployeeRole(@PathVariable("id") int roleId,
			@RequestBody EmployeeRole empRole) {

		empRoleService.updateEmployeeRole(empRole, roleId);
		EmployeeRole employeeRole = empRoleService.getEmployeeRoleById(roleId);

		return new ResponseEntity<EmployeeRole>(employeeRole, HttpStatus.OK);

	}
	
	//Delete
	@DeleteMapping("/{id}")
	public ResponseEntity<EmployeeRole> deleteEmployeeRole(@PathVariable("id") int roleId) {

		EmployeeRole empRole = empRoleService.getEmployeeRoleById(roleId);
		empRoleService.deleteEmployeeRole(roleId);

		return new ResponseEntity<EmployeeRole>(empRole, HttpStatus.OK);

	}

}